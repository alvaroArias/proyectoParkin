CREATE TABLE IF NOT EXISTS cliente(
id_cliente INT NOT NULL AUTO_INCREMENT,
id_plaza_disponible INT NOT NULL,
nombre VARCHAR(45) NOT NULL,
telefono VARCHAR(20) NOT NULL,
correo VARCHAR(45) NOT NULL,
PRIMARY KEY (id_cliente),
FOREIGN KEY (id_plaza_disponible) REFERENCES plaza_disponible (id_plaza_disponible))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS vehiculo(
id_vehiculo INT NOT NULL AUTO_INCREMENT,
id_cliente INT NOT NULL,
tipo_vehiculo VARCHAR(15) NOT NULL,
matricula VARCHAR(10) NOT NULL,
descripcion TEXT NOT NULL,
PRIMARY KEY (id_vehiculo),
FOREIGN KEY (id_cliente) REFERENCES cliente (id_cliente))
ENGINE = InnoDB;